#!/bin/bash
docker exec -t "hsl_2022_kobuki" /bin/bash -c ". /home/robot/starline_2022/kobuki/slave_ws/install/setup.bash && \
        ROS_DOMAIN_ID=5 ros2 launch slave start_launch.py"
