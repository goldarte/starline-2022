#!/bin/bash

# Written by Nikolay Dema <ndema2301@gmail.com>, September 2022

KOB_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
REPO_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

xhost +local:docker > /dev/null || true

IMG_NAME="hsl_2022_kobuki_sol"

mkdir -p ${REPO_ROOT}/.vscode-server/bin || true
mkdir -p ${REPO_ROOT}/.vscode-server/data || true


### DOCKER RUN ----------------------------------------------------------- #

docker run  -d -ti --rm \
            -e "DISPLAY=${DISPLAY}" \
            -e "QT_X11_NO_MITSHM=1" \
            -e XAUTHORITY \
            -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
            -v /etc/localtime:/etc/localtime:ro \
            -v ${KOB_ROOT}/slave_ws:/slave_ws \
            -v ${KOB_ROOT}/master_ws:/master_ws \
            -v ${REPO_ROOT}:/home/robot/starline_2022 \
            -v ~/.ssh:/home/robot/.ssh \
            -v /dev:/dev \
            -v ~/.Xauthority:/home/robot/.Xauthority \
            -v ${REPO_ROOT}/.vscode-server/bin:/home/robot/.vscode-server/bin \
            -v ${REPO_ROOT}/.vscode-server/data:/home/robot/.vscode-server/data \
            --mount 'source=vscode-server,target=/home/robot/.vscode-server/extensions,type=volume' \
            --net=host \
            --privileged \
            --name "hsl_2022_kobuki" ${IMG_NAME} \
            > /dev/null
