#!/bin/bash
docker exec -t "hsl_2022_kobuki" /bin/bash -c ". /home/robot/starline_2022/kobuki/master_ws/install/setup.bash && \
        ros2 launch resque sensors.launch.py"
