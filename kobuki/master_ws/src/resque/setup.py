from setuptools import setup
import os
from glob import glob

package_name = 'resque'

def generate_data_files():
    data_files = []
    data_dirs = ['launch', 'configs', 'params', 'maps']

    for _dir in data_dirs:
        for file_path in glob(_dir + '/**', recursive=True):
            file = os.path.split(file_path)
            if os.path.isfile(file_path):
                data_files.append((os.path.join('share', package_name, file[0]), [file_path]))

    return data_files

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name, 'resque/path_points'],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ] + generate_data_files(),
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='robot',
    maintainer_email='robot@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'slave_detector = resque.slave_detector:main',
            'slave_warden = resque.slave_warden:main',
            'explore_nash = resque.explore_nash:main',
            'resc_statem = resque.resc_statem:main',
            'resc_statem_mini = resque.resc_statem_mini:main',
            'resc_statem_mini_n = resque.resc_statem_mini_n:main',
            'resc_statem_maxi = resque.resc_statem_maxi:main',
        ],
    },
)
