from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data

from geometry_msgs.msg import PoseStamped
from nav2_simple_commander.robot_navigator import BasicNavigator, TaskResult
import rclpy
from .path_points.path_points import enter_maze_point, maze_points_maxi as maze_points
from std_msgs.msg import Bool
from tf2_ros import ExtrapolationException, ConnectivityException, LookupException
from copy import deepcopy

import numpy as np
from time import sleep

slave_found = False # global, is bad, yes

def get_quaternion_from_euler(roll, pitch, yaw):
  qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
  qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
  qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
  qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
  return [qx, qy, qz, qw]

def create_pose(navigator, point):
    pose = PoseStamped()
    pose.header.frame_id = 'map'
    pose.header.stamp = navigator.get_clock().now().to_msg()
    pose.pose.position.x = point[0]
    pose.pose.position.y = point[1]
    pose.pose.position.z = 0.0
    pose.pose.orientation.x = 0.0
    pose.pose.orientation.y = 0.0
    pose.pose.orientation.z = 0.0
    pose.pose.orientation.w = 1.0
    return pose    


class InitialState():
    def __init__(self, navigator):
        self.navigator = navigator
        self.current_pose = PoseStamped()
        self.initial_pose = None
        self.slave_pose = None

    def get_init_pose(self):
        initial_pose = PoseStamped()
        initial_pose.header.frame_id = 'map'
        initial_pose.header.stamp = self.navigator.get_clock().now().to_msg()
        initial_pose.pose.position.x = -1.8471
        initial_pose.pose.position.y = -2.29
        initial_pose.pose.position.z = 0.0
        qx, qy, qz, qw = get_quaternion_from_euler(0.000024, 0.006695, 1.616430)
        initial_pose.pose.orientation.x = -0.0008
        initial_pose.pose.orientation.y = 0.0007
        initial_pose.pose.orientation.z = 0.7076
        initial_pose.pose.orientation.w = 0.7065
        self.initial_pose = initial_pose
        self.slave_pose = deepcopy(self.initial_pose)
        self.slave_pose.pose.position.x -= 1
        return initial_pose

    def get_path(self, pose_1, pose_2):
        self.navigator.setInitialPose(pose_1)
        self.navigator.waitUntilNav2Active()
        path = self.navigator.getPath(pose_1, pose_2)
        return path

    def move_to_goal(self, goal_point):
        goal_pose = create_pose(self.navigator, goal_point)
        self.navigator.goToPose(goal_pose)
        while not self.navigator.isTaskComplete():
            ...
        result = self.navigator.getResult()
        return result

    def execute(self):
        print('Executing State InitialState')

        initial_pose = self.get_init_pose()
        self.current_pose = initial_pose

        # enter maze
        result = self.move_to_goal(enter_maze_point)
        if result == TaskResult.SUCCEEDED:
            print('In the maze')

        return 'entered_maze'

########################
class RescueNode(Node):
    def __init__(self):
        super().__init__("rescue_node")

        self.n = 0
        self.direction = 1
        self.points = maze_points
        self.found_slave = False

        self.navigator = BasicNavigator()

        self.navigator.waitUntilNav2Active()

        self.initial = InitialState(self.navigator)

        self.initial.execute()

        self.subscription = self.create_subscription(
            Bool,
            '/slave_visible',
            self.slave_callback, qos_profile_sensor_data)

        self.initial.execute()

        self.create_timer(1, self.simple_dimple)


    def slave_callback(self, data):
        if data.data:
            self.get_logger().info("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")
            print('nashelsya')
            if not self.found_slave:
                self.found_slave = True
                try:
                    t = self.tf_buffer.lookup_transform(
                        "map", "slave_footprint", rclpy.time.Time())
                    self.navigator.cancelTask()
                    # вот тут path надо обработать и послать
                except (LookupException, ConnectivityException, ExtrapolationException):
                    pass
                print('nashelsya')
                goal_pose = PoseStamped()
                goal_pose.header.frame_id = "map"
                goal_pose.pose.position.x = t.transform.translation.x
                goal_pose.pose.position.y = t.transform.translation.y
                goal_pose.header.stamp = self.navigator.get_clock().now().to_msg()

                self.navigator.goToPose(goal_pose)

    def simple_dimple(self):
        if self.found_slave:
            return
        if not self.navigator.isTaskComplete():
            feedback = self.navigator.getFeedback()
            if feedback.number_of_recoveries > 3:
                print('cancel from code')
                self.navigator.cancelTask()
                self.explore_busy = False
                self.point_to_inspect = None
                self.should_stop = False
            else:
                return
        else:
            goal_pose = create_pose(self.navigator, self.points[self.n])
            self.navigator.goToPose(goal_pose)
            self.n += self.direction
            if self.n == 0 or self.n == len(self.points)-1:
                self.direction = -self.direction


# main
def main(args=None):

    # print("resc_statem")
    # print('sleeping to let nav stack init')
    # sleep(10)

    rclpy.init(args=args)

    node = RescueNode()
    rclpy.spin(node)
    node.destroy_node()

    rclpy.shutdown()


if __name__ == "__main__":
    main()
