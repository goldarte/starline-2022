from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data

from geometry_msgs.msg import PoseStamped
from nav2_simple_commander.robot_navigator import BasicNavigator, TaskResult
import rclpy
from .path_points.path_points import enter_maze_point, maze_points_mini as maze_points
from std_msgs.msg import Bool
from geometry_msgs.msg import Twist, PoseStamped
from nav_msgs.msg import Odometry, OccupancyGrid
from nav2_simple_commander.robot_navigator import BasicNavigator
from tf2_ros.buffer import Buffer
from tf2_ros import ExtrapolationException, ConnectivityException, LookupException
from tf2_ros.transform_listener import TransformListener
from copy import deepcopy
import math

from websocket import create_connection
from time import sleep
from json import dumps

ws = None
def connect_ws():
    global ws
    try:
        # ws = create_connection("ws://localhost:6969/")
        ws = create_connection("ws://192.168.4.5:6969/")
    except ConnectionRefusedError:
        sleep(1)
        print('Reconnecting')
        connect_ws()
    except OSError:
        sleep(1)
        print('Reconnecting')
        connect_ws()
def send_data(data):
    global ws
    try:
        ws.send(dumps(data))
    except BrokenPipeError:
        ws.close()
        sleep(1)
        connect_ws()       
        send_data(data)

import numpy as np
from time import sleep
import time

slave_found = False # global, is bad, yes

def euler_from_quaternion(x, y, z, w):
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)

    return roll_x, pitch_y, yaw_z

def poseStamptToPoint(data):
    _, _, r = euler_from_quaternion(data.pose.orientation.x, data.pose.orientation.y, data.pose.orientation.z, data.pose.orientation.w)
    t = {
        'x': data.pose.position.x,
        'y': data.pose.position.y,
        'r': r}
    return t

def rot_from_poses(pose1, pose2):
    return math.atan2(pose2.pose.position.x-pose1.pose.position.x, pose2.pose.position.y-pose1.pose.position.y)

def rot_from_xy(x1, y1, x2, y2):
    return math.atan2(x2-x1, y2-y1)

def get_quaternion_from_euler(roll, pitch, yaw):
  qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
  qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
  qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
  qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
  return [qx, qy, qz, qw]

def create_pose(navigator, point):
    pose = PoseStamped()
    pose.header.frame_id = 'map'
    pose.header.stamp = navigator.get_clock().now().to_msg()
    pose.pose.position.x = point[0]
    pose.pose.position.y = point[1]
    pose.pose.position.z = 0.0
    pose.pose.orientation.x = 0.0
    pose.pose.orientation.y = 0.0
    pose.pose.orientation.z = 0.0
    pose.pose.orientation.w = 1.0
    return pose    


class InitialState():
    def __init__(self, navigator):
        self.navigator = navigator
        self.current_pose = PoseStamped()
        self.initial_pose = None
        self.slave_pose = None

    def get_init_pose(self):
        initial_pose = PoseStamped()
        initial_pose.header.frame_id = 'map'
        initial_pose.header.stamp = self.navigator.get_clock().now().to_msg()
        initial_pose.pose.position.x = -1.8471
        initial_pose.pose.position.y = -2.29
        initial_pose.pose.position.z = 0.0
        qx, qy, qz, qw = get_quaternion_from_euler(0.000024, 0.006695, 1.616430)
        initial_pose.pose.orientation.x = -0.0008
        initial_pose.pose.orientation.y = 0.0007
        initial_pose.pose.orientation.z = 0.7076
        initial_pose.pose.orientation.w = 0.7065
        self.initial_pose = initial_pose
        self.slave_pose = deepcopy(self.initial_pose)
        self.slave_pose.pose.position.x -= 1
        return initial_pose

    def get_path(self, pose_1, pose_2):
        self.navigator.setInitialPose(pose_1)
        self.navigator.waitUntilNav2Active()
        path = self.navigator.getPath(pose_1, pose_2)
        return path

    def move_to_goal(self, goal_point):
        goal_pose = create_pose(self.navigator, goal_point)
        self.navigator.goToPose(goal_pose)
        while not self.navigator.isTaskComplete():
            ...
        result = self.navigator.getResult()
        return result

    def execute(self):
        print('Executing State InitialState')

        initial_pose = self.get_init_pose()
        self.current_pose = initial_pose

        # enter maze
        result = self.move_to_goal(enter_maze_point)
        if result == TaskResult.SUCCEEDED:
            print('In the maze')

        return 'entered_maze'

########################
class RescueNode(Node):
    def __init__(self):
        super().__init__("rescue_node")

        self.n = 0
        self.direction = 1
        self.points = maze_points
        self.found_slave = False
        self.second_round = False

        self.navigator = BasicNavigator()

        self.navigator.waitUntilNav2Active()

        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)

        self.initial = InitialState(self.navigator)

        self.initial.execute()

        self.subscription = self.create_subscription(
            Bool,
            '/slave_visible',
            self.slave_callback, qos_profile_sensor_data)

        self.initial.execute()

        self.create_timer(1, self.simple_dimple)


    def slave_callback(self, data):
        if self.second_round:
            return
        if data.data:
            self.second_round = True
            self.get_logger().info("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")
            print('nashelsya')
            goal_t = {'x': 0, 'y': 0, 'z': 0}
            if not self.found_slave:
                self.found_slave = True
                try:
                    self.destroy_timer(self.simple_dimple)
                except:
                    pass
                self.navigator.cancelTask()
                start = time.time()
                count = 0
                while time.time() - start < 5.0:
                    try:
                        t = self.tf_buffer.lookup_transform(
                            "map", "slave_footprint", rclpy.time.Time())
                        m = self.tf_buffer.lookup_transform(
                            "map", "base_footprint", rclpy.time.Time())
                        # вот тут path надо обработать и послать
                    except (LookupException, ConnectivityException, ExtrapolationException):
                        pass
                    else:
                        goal_pose = PoseStamped()
                        goal_pose.header.frame_id = "map"
                        goal_pose.pose.position.x = t.transform.translation.x
                        goal_pose.pose.position.y = t.transform.translation.y
                        goal_pose.pose.position.z = t.transform.translation.y
                        goal_pose.pose.orientation.x = t.transform.rotation.x
                        goal_pose.pose.orientation.y = t.transform.rotation.y
                        goal_pose.pose.orientation.z = t.transform.rotation.z
                        goal_pose.pose.orientation.w = t.transform.rotation.w
                        goal = poseStamptToPoint(goal_pose)
                        goal_t['x'] += goal['x']
                        goal_t['y'] += goal['y']
                        goal_t['r'] += goal['r']
                        count += 1.0
                    time.sleep(0.05)
                if count > 0:
                    goal_t['x'] /= count
                    goal_t['y'] /= count
                    goal_t['r'] /= count

                master_pose = PoseStamped()
                master_pose.header.frame_id = "map"
                master_pose.pose.position.x = m.transform.translation.x
                master_pose.pose.position.y = m.transform.translation.y
                master_pose.pose.position.z = m.transform.translation.y
                master_pose.pose.orientation.x = m.transform.rotation.x
                master_pose.pose.orientation.y = m.transform.rotation.y
                master_pose.pose.orientation.z = m.transform.rotation.z
                master_pose.pose.orientation.w = m.transform.rotation.w

                master_t = poseStamptToPoint(master_pose)

                delta_rot = rot_from_poses(master_pose, goal_pose)
                q = get_quaternion_from_euler(0,0,delta_rot)
                dx = goal_t['x']-master_t['x']
                dy = goal_t['y']-master_t['y']
                slave_pose = PoseStamped()
                slave_pose.header.frame_id = "map"
                slave_pose.pose.position.x = master_pose.pose.position.x + dx/3.0
                slave_pose.pose.position.y = master_pose.pose.position.y + dy/3.0
                slave_pose.pose.position.z = 0.0
                slave_pose.pose.orientation.x = q[0]
                slave_pose.pose.orientation.y = q[1]
                slave_pose.pose.orientation.z = q[2]
                slave_pose.pose.orientation.w = q[3]
                slave_pose.header.stamp = self.navigator.get_clock().now().to_msg()

                self.navigator.goToPose(slave_pose)

                sleep(5.0)

                self.get_logger().info("EEEEEEEEE")
                if self.direction > 0:
                    master_points = list(reversed(self.points[:self.n]))
                else:
                    master_points = list(reversed(self.points[self.n:]))
                slave_points = [{'x': 0.0, 'y': 0.0, 'r' : math.pi - delta_rot - goal['r']}]
                slave_points.append({'x': math.sqrt(dx*dx+dy*dy), 'y': 0.0, 'r': 0.0})
                slave_points.append({'x': 0.0, 'y': 0.0, 'r' : -(math.pi - delta_rot) + rot_from_xy(master_t['x'], master_t['y'],master_points[0][0],master_points[0][1])})
                dxm = master_points[0][0] - master_t['x']
                dym = master_points[0][1] - master_t['y']
                last_rot = rot_from_xy(master_t['x'], master_t['y'],master_points[0][0],master_points[0][1])
                slave_points.append({'x': math.sqrt(dxm*dxm+dym*dym), 'y': 0.0, 'r': 0.0})
                for i in (1, len(master_points)-1):
                    dxs = master_points[i-1][0] - master_points[i][0]
                    dys = master_points[i-1][1] - master_points[i][1]
                    rot = rot_from_xy(master_points[i-1][0],master_points[i-1][1], master_points[i][0],master_points[i][1])
                    slave_points.append({'x': 0.0, 'y': 0.0, 'r': rot-last_rot})
                    last_rot = rot
                    slave_points.append({'x': math.sqrt(dxs*dxs+dys*dys), 'y': 0.0, 'r': 0.0})

                dxs = master_points[-2][0] - master_points[-1][0] + 1
                dys = master_points[-2][1] - master_points[-1][1] + 3
                rot = rot_from_xy(master_points[-2][0],master_points[-2][1], self.points[0][0] - 1,self.points[0][1]-3)
                slave_points.append({'x': 0.0, 'y': 0.0, 'r': rot-last_rot})
                last_rot = rot
                slave_points.append({'x': math.sqrt(dxs*dxs+dys*dys), 'y': 0.0, 'r': 0.0})

                send_data(slave_points)

                goal_pose = create_pose(self.navigator, self.points[0])

                self.points = master_points
                self.found_slave = False
                self.direction = 1.0
                self.n = 0
                self.create_timer(1, self.simple_dimple)

    def simple_dimple(self):
        if self.found_slave:
            return
        if not self.navigator.isTaskComplete():
            feedback = self.navigator.getFeedback()
            if feedback.number_of_recoveries > 3:
                print('cancel from code')
                self.navigator.cancelTask()
                self.explore_busy = False
                self.point_to_inspect = None
                self.should_stop = False
            else:
                return
        else:
            goal_pose = create_pose(self.navigator, self.points[self.n])
            self.navigator.goToPose(goal_pose)
            self.n += self.direction
            if self.n == 0 or self.n == len(self.points)-1:
                self.direction = -self.direction


# main
def main(args=None):

    # print("resc_statem")
    # print('sleeping to let nav stack init')
    # sleep(10)

    rclpy.init(args=args)

    node = RescueNode()
    rclpy.spin(node)
    node.destroy_node()

    rclpy.shutdown()


if __name__ == "__main__":
    main()
