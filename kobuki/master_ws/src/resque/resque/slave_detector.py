import rclpy
from rclpy.node import Node
from astra_camera_msgs.srv import GetCameraParams
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from tf2_ros import TransformBroadcaster
from geometry_msgs.msg import TransformStamped
from std_msgs.msg import Bool

import cv2 as cv
import numpy as np
import math


# вот это надо от робота получать конечно
camera_matrix = None
distortion = None

arucoDict = cv.aruco.Dictionary_get(cv.aruco.DICT_4X4_50)

markers = {
    "0": {"size": 0.08, "t": [0.0, 0.157, 0.2475], "r": [math.pi / 2, 0.0, math.pi]},
    "1": {"size": 0.08, "t": [0.0, -0.157, 0.2475], "r": [-math.pi / 2, 0.0, 0.0]},
    "2": {"size": 0.08, "t": [-0.155, 0.0, 0.357], "r": [-math.pi / 2, math.pi / 2, 0.0]},
    "3": {"size": 0.08, "t": [0.13, 0.0, 0.192], "r": [-math.pi / 2,  -math.pi / 2, 0.0]},
    "6": {"size": 0.05, "t": [0.1, -0.126, 0.165], "r": [-math.pi/2, -math.pi/6, 0.0]},
    "7": {"size": 0.05, "t": [0.1, 0.126, 0.165], "r": [-math.pi/2, -5*math.pi/6, 0.0]}
}


def eul2rot(theta):
    R_x = np.array([[1.0, 0.0, 0.0],
                    [0.0, math.cos(theta[0]), -math.sin(theta[0])],
                    [0.0, math.sin(theta[0]), math.cos(theta[0])]
                    ])

    R_y = np.array([[math.cos(theta[1]), 0.0, math.sin(theta[1])],
                    [0.0, 1.0, 0.0],
                    [-math.sin(theta[1]), 0.0, math.cos(theta[1])]
                    ])

    R_z = np.array([[math.cos(theta[2]), -math.sin(theta[2]), 0.0],
                    [math.sin(theta[2]), math.cos(theta[2]), 0.0],
                    [0.0, 0.0, 1.0]
                    ])

    R = np.dot(R_z, np.dot(R_y, R_x))

    return R

def rotationMatrixToQuaternion1(m):
    #q0 = qw
    t = np.matrix.trace(m)
    q = np.asarray([0.0, 0.0, 0.0, 0.0], dtype=np.float64)

    if(t > 0):
        t = np.sqrt(t + 1)
        q[3] = 0.5 * t
        t = 0.5/t
        q[0] = (m[2,1] - m[1,2]) * t
        q[1] = (m[0,2] - m[2,0]) * t
        q[2] = (m[1,0] - m[0,1]) * t

    else:
        i = 0
        if (m[1,1] > m[0,0]):
            i = 1
        if (m[2,2] > m[i,i]):
            i = 2
        j = (i+1)%3
        k = (j+1)%3

        t = np.sqrt(m[i,i] - m[j,j] - m[k,k] + 1)
        q[i] = 0.5 * t
        t = 0.5 / t
        q[3] = (m[k,j] - m[j,k]) * t
        q[j] = (m[j,i] + m[i,j]) * t
        q[k] = (m[k,i] + m[i,k]) * t

    return q

def process_markers():
    for k in markers.keys():
        markers[k]["rm"] = eul2rot(markers[k]["r"])
        markers[k]["rmt"] = markers[k]["rm"].dot(-np.array(markers[k]["t"]))

process_markers()

# а нахуй картинку постить кстати
class ImagePublisher(Node):

    def __init__(self):
        super().__init__('slave_detector_pub')
        self.publisher_ = self.create_publisher(
            Image, '/slave_detector/image', 10)

    def publish_image(self, msg):
        self.publisher_.publish(msg)


class ImageSubscriber(Node):

    def __init__(self):
        super().__init__('slave_detector_sub')
        qos_policy = rclpy.qos.QoSProfile(reliability=rclpy.qos.ReliabilityPolicy.BEST_EFFORT,
                                          history=rclpy.qos.HistoryPolicy.KEEP_LAST,
                                          depth=10)
        self.subscription = self.create_subscription(
            Image,
            '/camera/color/image_raw',  # вот это узнать конечно
            self.image_callback,
            qos_profile=qos_policy)
        self.br = CvBridge()
        self.image_publisher = ImagePublisher()
        self.subscription
        self.iter = 0

        self.cli = self.create_client(
            GetCameraParams, "/camera/get_camera_params")
        self.req = GetCameraParams.Request()

        self.tf_broadcaster = TransformBroadcaster(self)
        self.publisher = self.create_publisher(
            Bool, '/slave_visible', 10)

    def send_request(self):
        self.future = self.cli.call_async(self.req)
        rclpy.spin_until_future_complete(self, self.future)
        return self.future.result()

    def image_callback(self, msg):
        global markers, camera_matrix, distortion
        if type(camera_matrix) != np.ndarray: return
        if type(distortion) != np.ndarray: return
        self.iter += 1
        if self.iter < 2:
            return
        self.iter = 0
        frame = self.br.imgmsg_to_cv2(msg, "bgr8")
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        markers_corners, ids, rejected = cv.aruco.detectMarkers(
            gray, arucoDict)
        cv.aruco.drawDetectedMarkers(frame, markers_corners, ids)

        if np.all(ids is not None):
            points = []

            for corners, marker_id in zip(markers_corners, ids):
                if str(marker_id[0]) in markers.keys():
                    marker = markers[str(marker_id[0])]
                    rvec, tvec, _ = cv.aruco.estimatePoseSingleMarkers(
                        corners, marker["size"], camera_matrix, distortion)
                    rv = rvec[0][0]
                    tv = tvec[0][0]
                    rm, _ = cv.Rodrigues(rv)
                    tv += rm.dot(marker["rmt"])  # x - red, y - green, z - blue
                    rm = rm.dot(marker["rm"])
                    points.append([tv, rm])

            avg_t = np.array([0.0, 0.0, 0.0])
            avg_rm = None
            avg_c = 0

            for point in points:
                # вот тут можно как-то отфильтровать 
                avg_t += point[0]
                if avg_c == 0:
                    avg_rm = point[1]
                else:
                    avg_rm += point[1]
                avg_c += 1

            if avg_rm == None: return

            pose = [avg_t / avg_c, avg_rm / avg_c]

            if avg_c > 0:
                cv.drawFrameAxes(frame, camera_matrix,
                                 distortion, pose[1], pose[0], 0.1)

            # ну и тут надо расчехлить tf2 (мы в camera_color_optical_frame)
            t = TransformStamped()

            t.header.stamp = self.get_clock().now().to_msg()
            t.header.frame_id = 'camera_color_optical_frame'
            t.child_frame_id = 'slave_footprint'

            t.transform.translation.x = pose[0][0]
            t.transform.translation.y = pose[0][1]
            t.transform.translation.z = pose[0][2]

            q = rotationMatrixToQuaternion1(pose[1])
            t.transform.rotation.x = q[0]
            t.transform.rotation.y = q[1]
            t.transform.rotation.z = q[2]
            t.transform.rotation.w = q[3]

            # Send the transformation
            self.tf_broadcaster.sendTransform(t)
            # self.image_publisher.publish_image(self.br.cv2_to_imgmsg(frame))
            b = Bool()
            b.data = True
            self.publisher.publish(b)
        else:
            b = Bool()
            b.data = False
            self.publisher.publish(b)

    def stop(self):
        self.image_publisher.destroy_node()


def main(args=None):
    global camera_matrix, distortion
    rclpy.init(args=args)

    image_subscriber = ImageSubscriber()
    success = False

    while not success:
        print('Requesting')
        response: GetCameraParams.Response = image_subscriber.send_request()
        success = response.success

    print('Request OK')
    camera_matrix = np.array([[response.r_intr_p[0], 0.0, response.r_intr_p[2]], [
                             0.0, response.r_intr_p[1], response.r_intr_p[3]], [0.0, 0.0, 1.0]])
    distortion = np.array([response.r_k])
    rclpy.spin(image_subscriber)

    image_subscriber.stop()
    image_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
