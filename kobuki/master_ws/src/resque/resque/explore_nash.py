import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from geometry_msgs.msg import Twist, PoseStamped
from nav_msgs.msg import Odometry, OccupancyGrid
from nav2_simple_commander.robot_navigator import BasicNavigator
from tf2_ros.buffer import Buffer
from tf2_ros import ExtrapolationException, ConnectivityException, LookupException
from tf2_ros.transform_listener import TransformListener
from std_msgs.msg import Bool
import numpy as np
import math
from collections import deque
from copy import deepcopy
from time import sleep

nav = None


def pouring_search(array, _x, _y):

    _array = np.copy(array)
    #_array = array

    unknown_code = -1
    known_code = 0
    wall_code = 100
    tested_code = 103

    # col = [-1, -1, -1, 0, 0, 1, 1, 1]
    # row = [-1, 0, 1, -1, 1, -1, 0, 1]
    col = [-1, 0, 0, 1]
    row = [0, -1, 1,  0]

    def isSafe(mat, y, x):
        return (0 <= y < len(mat)) and (0 <= x < len(mat[0])) and (mat[y][x] == known_code)

    def sort_key(a):
        return (a[1] - _x)**2 + (a[0] - _y)**2

    q = deque()
    q.append((_y, _x))

    if (_array[_y][_x] == unknown_code):
        return [_y, _x]

    while q:
        q = deque(sorted(list(q), key=sort_key))
        y, x = q.popleft()

        _array[y][x] = tested_code

        for k in range(len(row)):
            if (_array[y + row[k], x + col[k]] == unknown_code):
                return (y + row[k], x + col[k])
            if isSafe(_array, y + row[k], x + col[k]) and not ((y + row[k], x + col[k]) in q):
                q.append((y + row[k], x + col[k]))

    return [-1, -1]


def has_path(_array, x1, y1, x2, y2):
    m = _array.copy()

    c = 1

    dirs = [[0, 1], [0, -1], [1, 0], [-1, 0]]

    def isSafe(mat, x, y):
        return (0 <= y < len(mat)) and (0 <= x < len(mat[y]))

    m[y1][x1] = 5

    while c > 0:
        c = 0
        for y in range(0, len(m)):
            for x in range(0, len(m[y])):
                if m[y][x] == 5:
                    for dir in dirs:
                        _x, _y = x + dir[0], y + dir[1]
                        if isSafe(m, _x, _y):
                            if y == y2 and x == x2:
                                return True
                            if m[_y][_x] == 0:
                                m[_y][_x] = 5
                                c += 1
    return False


def euler_from_quaternion(x, y, z, w):
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)

    return roll_x, pitch_y, yaw_z

def poseStamptToPoint(data):
    _, _, r = euler_from_quaternion(data.pose.orientation.x, data.pose.orientation.y, data.pose.orientation.z, data.pose.orientation.w)
    t = {
        'x': data.pose.position.x,
        'y': data.pose.position.y,
        'r': r}
    return t


'''
    Eto tebe ndo
'''
def pathAss(data):
    angle_threshold = math.pi / 18

    simpled_path = [poseStamptToPoint(data.poses[0])]
    yaw_prev = simpled_path[0]['r']

    for i in data.poses[1:]:
        t = poseStamptToPoint(i)
        if abs(t['r'] - yaw_prev <= angle_threshold):
            simpled_path.append(t)
            yaw_prev = t['r']
        
    return simpled_path


def quaternion_to_rotation_matrix(Q):
    # Extract the values from Q
    q0 = Q.x
    q1 = Q.y
    q2 = Q.z
    q3 = Q.w

    # First row of the rotation matrix
    r00 = 2 * (q0 * q0 + q1 * q1) - 1
    r01 = 2 * (q1 * q2 - q0 * q3)
    r02 = 2 * (q1 * q3 + q0 * q2)

    # Second row of the rotation matrix
    r10 = 2 * (q1 * q2 + q0 * q3)
    r11 = 2 * (q0 * q0 + q2 * q2) - 1
    r12 = 2 * (q2 * q3 - q0 * q1)

    # Third row of the rotation matrix
    r20 = 2 * (q1 * q3 - q0 * q2)
    r21 = 2 * (q2 * q3 + q0 * q1)
    r22 = 2 * (q0 * q0 + q3 * q3) - 1

    # 3x3 rotation matrix
    rot_matrix = np.array([[r00, r01, r02],
                           [r10, r11, r12],
                           [r20, r21, r22]])

    return rot_matrix


def tranform_to_matrix(t):
    rot_mat = quaternion_to_rotation_matrix(t.rotation)
    return np.array([[rot_mat[0][0], rot_mat[1][0], rot_mat[2][0], t.translation.x],
                     [rot_mat[0][1], rot_mat[1][1], rot_mat[2][1], t.translation.y],
                     [rot_mat[0][2], rot_mat[1][2], rot_mat[2][2], t.translation.z],
                     [0.0, 0.0, 0.0, 1.0]])


def apply_transform(t, pose):

    return tranform_to_matrix(t.transform).dot(np.array([pose.position.x, pose.position.y, pose.position.z, 1.0]))


'''
            ,.     ,.
            {^ \-"-/ ^}
            "   """   "
           { <O> _ <O> }
           ==_ .:Y:. _==
         .""  `--^--' "".
        (,~-~."" "" ,~-~.)
  ------(     )----(     )-----
        ^-'-'-^    ^-'-'-^
  _____________________________
        |"""" /~.^.~\ """"|
  hjw    ,i-i-i(""(  i-i-i.
  `97   (o o o ))"")( o o o)
         \(_) /(""(  \ (_)/
          `--'  \""\  `--'
                 )"")
                (""/
                 `"
'''


def normilize_angle(_a):
    a = _a
    while a > math.pi:
        a -= 2 * math.pi
    while a < -math.pi:
        a += 2 * math.pi
    return a


def odometry_to_pose(odom):
    return odom.pose.pose


start_position = None


def point_to_coords(mapa, xy):
    origin = mapa.info.origin
    orientation = np.array([origin.orientation.x, origin.orientation.y,
                            origin.orientation.z, origin.orientation.w])
    theta = euler_from_quaternion(*orientation)[2]
    info = mapa.info

    R = np.array([[math.cos(theta), math.sin(theta)],
                  [-math.sin(theta), math.cos(theta)]])
    return R.dot(info.resolution * xy) + np.array([origin.position.x, origin.position.y])


def coords_to_point(mapa, xy):
    origin = mapa.info.origin
    orientation = np.array([origin.orientation.x, origin.orientation.y,
                            origin.orientation.z, origin.orientation.w])
    theta = euler_from_quaternion(*orientation)[2]
    info = mapa.info

    R = np.array([[math.cos(theta), math.sin(theta)],
                  [-math.sin(theta), math.cos(theta)]])

    R = np.linalg.inv(R)

    # [x;y] = R * [px * resolution ; py * resolution] + [x0,y0]
    # [px; py] = (invR * ([x; y] - [x0, y0])) / resolution
    return ((R.dot(xy - np.array([origin.position.x, origin.position.y]))) / info.resolution).round().astype(int)


class MoveForward(Node):

    def __init__(self, dist):
        super().__init__('Mover')
        self.publisher = self.create_publisher(Twist, '/commands/velocity', 10)
        # self.publisher = self.create_publisher(Twist, '/cmd_vel', 10)

        self.odom_subscription = self.create_subscription(
            Odometry,
            '/odom',
            self.odometry_callback,
            qos_profile_sensor_data)

        self.target_pose = np.array([dist, 0.0])
        self.dist_to_travel = dist
        self.target_angle = 0.0
        self.end_angle = 0.0
        self.start_pose = None
        self.start_angle = None
        self.odo_pose = None
        self.start_angle_reached = False
        self.dist_reached = False

    def odometry_callback(self, data):
        print('Moving')
        self.odo_pose = np.array(
            [data.pose.pose.position.x, data.pose.pose.position.y])
        orientation = np.array([data.pose.pose.orientation.x, data.pose.pose.orientation.y,
                               data.pose.pose.orientation.z, data.pose.pose.orientation.w])
        self.odo_angle = normilize_angle(
            euler_from_quaternion(*orientation)[2])

        if type(self.start_pose) != np.ndarray:
            global start_position
            start_position = odometry_to_pose(data)
            self.start_pose = self.odo_pose
            self.start_angle = self.odo_angle

        if type(self.target_pose) == np.ndarray:
            movement = Twist()
            if self.dist_to_travel == 0.0 and self.target_angle == 0.0:
                self.target_pose = None
                movement.linear.x = 0.0
                movement.angular.z = 0.0
            else:
                diff = self.target_angle - \
                    normilize_angle(self.odo_angle - self.start_angle)
                diff = normilize_angle(diff)
                if abs(diff) > 0.005 and not self.start_angle_reached:
                    # print(diff, self.odo_angle, self.start_angle)
                    if normilize_angle(self.odo_angle - normilize_angle(self.start_angle + self.target_angle)) > 0:
                        movement.angular.z = -max(min(diff * 2, 0.8), 0.4)
                    else:
                        movement.angular.z = max(min(diff * 2, 0.8), 0.4)

                    movement.linear.x = 0.0
                else:
                    self.start_angle_reached = True
                    dist = self.dist_to_travel - \
                        np.linalg.norm(self.odo_pose-self.start_pose)
                    if dist > 0.01 and not self.dist_reached:
                        # print(dist)
                        movement.linear.x = max(min(dist * 0.3, 1), 0.15)
                        movement.angular.z = 0.0
                    else:
                        self.dist_reached = True
                        diff = self.end_angle - \
                            normilize_angle(self.odo_angle - self.start_angle)
                        diff = normilize_angle(diff)
                        if abs(diff) > 0.005:
                            # print(diff, self.odo_angle, self.start_angle)
                            movement.linear.x = 0.0

                            if normilize_angle(self.odo_angle - normilize_angle(self.start_angle + self.end_angle)) > 0:
                                movement.angular.z = - \
                                    max(min(diff * 2, 0.8), 0.4)
                            else:
                                movement.angular.z = max(
                                    min(diff * 2, 0.8), 0.4)
                        else:
                            movement.linear.x = 0.0
                            movement.angular.z = 0.0
                            self.publisher.publish(movement)
                            raise BaseException

            self.publisher.publish(movement)


class Susanin(Node):
    def __init__(self):
        super().__init__('susanin')
        self.odom_subscription = self.create_subscription(
            Odometry,
            '/odom',
            self.odometry_callback,
            qos_profile_sensor_data)
        self.susanin_timer = self.create_timer(1, self.susanin_logic)
        self.goal_timer = self.create_timer(0.1, self.check_goal)

        self.map_subscription = self.create_subscription(
            OccupancyGrid,
            '/map',
            self.map_callback,
            qos_profile_sensor_data)

        self.slave_subscription = self.create_subscription(
            Bool,
            '/slave_visible',
            self.slave_callback,
            10)

        self.publisher = self.create_publisher(
            OccupancyGrid, '/processed_map', 10)

        self.explore_start_pose = None
        self.explore_busy = False
        self.should_stop = False
        self.mapa = None
        self.current_pose = None
        self.point_to_inspect = None
        self.block_center = None
        self.found_slave = False
        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.inspected_points = []
        self.master_base_pose = None
        self.slave_base_pose = None

    def odometry_callback(self, data):
        if type(self.explore_start_pose) == type(None):
            self.explore_start_pose = data
        self.update_position()

    def slave_callback(self, data):
        global nav
        print(data.data)
        if data.data:
            if not self.found_slave:
                self.found_slave = True
                print('FOUND SLAVE')
                sleep(2)
                goal_pose = PoseStamped()
                goal_pose.header.frame_id = "map"
                goal_pose.pose.position.x = self.master_base_pose[0]
                goal_pose.pose.position.y = self.master_base_pose[1]
                goal_pose.header.stamp = nav.get_clock().now().to_msg()
                self.explore_busy = True
                print(goal_pose)
                nav.cancelTask()
                nav.goToPose(goal_pose)
                try:
                    t = self.tf_buffer.lookup_transform(
                        "map", "slave_footprint", rclpy.time.Time())
                    slave_goal_pose = PoseStamped()
                    slave_goal_pose.header.frame_id = "map"
                    slave_goal_pose.pose.position.x = self.slave_base_pose[0]
                    slave_goal_pose.pose.position.y = self.slave_base_pose[1]
                    slave_goal_pose.header.stamp = nav.get_clock().now().to_msg()
                    
                    slave_start_pose = PoseStamped()
                    slave_start_pose.header.frame_id = "map"
                    slave_start_pose.pose.position.x = t.transform.translation.x
                    slave_start_pose.pose.position.y = t.transform.translation.y
                    slave_start_pose.pose.orientation = t.transform.rotation
                    slave_start_pose.header.stamp = nav.get_clock().now().to_msg()

                    path = nav.getPath(slave_start_pose, slave_goal_pose)
                    print(path)
                    
                    # вот тут path надо обработать и послать
                except (LookupException, ConnectivityException, ExtrapolationException) as e:
                    print(e)
                except Exception as e:
                    print(e)
            print('nashelsya!')
                

    def update_position(self):
        try:
            t = self.tf_buffer.lookup_transform(
                "map", "base_link", rclpy.time.Time())
            if self.current_pose == None or self.current_pose.x != t.transform.translation.x or self.current_pose.y != t.transform.translation.y:
                self.current_pose = t.transform.translation
                if self.mapa != None:
                    self.self_point = coords_to_point(self.mapa, np.array(
                        [self.current_pose.x, self.current_pose.y]))

            if type(self.block_center) != np.ndarray:
                orientation = t.transform.rotation
                orientation = np.array(
                    [orientation.x, orientation.y, orientation.z, orientation.w])
                theta = -euler_from_quaternion(*orientation)[2]
                R = np.array([[math.cos(theta), math.sin(theta)],
                              [-math.sin(theta), math.cos(theta)]])
                self.block_center = np.array(
                    [self.current_pose.x, self.current_pose.y])
                # вот это говно я подогнал, может быть надо -theta давать
                self.master_base_pose = self.block_center + R.dot(np.array([-1, 0]))
                self.slave_base_pose = self.block_center + R.dot(np.array([-1, 1]))
                self.block_center += R.dot(np.array([-10, 0]))
                print('set block center')
                self.update_processed_map()
        except (LookupException, ConnectivityException, ExtrapolationException):
            print('waiting for odom position')

    def check_goal(self):
        global nav
        if self.explore_busy:
            if self.should_stop:
                nav.cancelTask()
                self.explore_busy = False
                self.point_to_inspect = None
                self.should_stop = False
            else:
                if not nav.isTaskComplete():
                    feedback = nav.getFeedback()
                    if feedback.number_of_recoveries > 3 and not self.found_slave:
                        print('cancel from code')
                        self.inspected_points.append(self.point_to_inspect)
                        nav.cancelTask()
                        self.explore_busy = False
                        self.point_to_inspect = None
                        self.should_stop = False
                    else:
                        return
                elif not self.found_slave:
                    print('canceled itself(')
                    self.inspected_points.append(self.point_to_inspect)
                    self.explore_busy = False
                    self.point_to_inspect = None
                    self.should_stop = False

    def susanin_logic(self):
        global nav
        if self.mapa == None or type(self.block_center) != np.ndarray or self.found_slave:
            return

        if self.explore_busy:
            return
        # вот тут мы выбираем нужную точку

        x_y_inspect = np.array(pouring_search(
            self.processed_mapa, self.self_point[0], self.self_point[1]))
        processed_xy = np.array([x_y_inspect[1], x_y_inspect[0]])

        def check_in_points(self, point):
            for p in self.inspected_points:
                p = coords_to_point(self.mapa, p)
                if p[0] == point[0] and p[1] == point[1]:
                    return True
            return False

        while check_in_points(self, processed_xy):
            self.processed_mapa[processed_xy[1]][processed_xy[0]] = 0
            x_y_inspect = np.array(pouring_search(
                self.processed_mapa, self.self_point[0], self.self_point[1]))
            processed_xy = np.array([x_y_inspect[1], x_y_inspect[0]])

        print('Navigating', self.self_point, '=>', processed_xy)
        self.point_to_inspect = point_to_coords(self.mapa, processed_xy)
        goal_pose = PoseStamped()
        goal_pose.header.frame_id = "map"
        goal_pose.pose.position.x = self.point_to_inspect[0]
        goal_pose.pose.position.y = self.point_to_inspect[1]
        goal_pose.header.stamp = nav.get_clock().now().to_msg()

        nav.goToPose(goal_pose)
        print('Setted new goal')
        self.explore_busy = True

    def update_processed_map(self):
        if self.mapa == None:
            print('map is not yet available')
            return
        origin = self.mapa.info.origin
        orientation = np.array([origin.orientation.x, origin.orientation.y,
                               origin.orientation.z, origin.orientation.w])
        theta = euler_from_quaternion(*orientation)[2]
        info = self.mapa.info
        w = info.width
        h = info.height
        # map занято :<

        R = np.array([[math.cos(theta), math.sin(theta)],
                     [-math.sin(theta), math.cos(theta)]])
        map_data = np.reshape(self.mapa.data, (h, w))
        self.processed_mapa = map_data.copy()

        expand_radius = 5

        expand_mask = []

        for x in range(-expand_radius, expand_radius + 1):
            for y in range(-expand_radius, expand_radius + 1):
                if x ** 2 + y ** 2 <= expand_radius ** 2:
                    expand_mask.append([x, y])

        if type(self.block_center) == np.ndarray:
            for x in range(0, w):  # как же оптимизировать хочется
                for y in range(0, h):
                    if map_data[y][x] == 100:
                        for mask in expand_mask:
                            _x = x + mask[0]
                            _y = y + mask[1]
                            if (0 <= _x < w) and (0 <= _y < h):
                                self.processed_mapa[_y][_x] = 100

                    coords = R.dot(
                        info.resolution * np.array([x, y])) + np.array([origin.position.x, origin.position.y])
                    # 10^2
                    if (self.block_center[0] - coords[0]) ** 2 + (self.block_center[1] - coords[1]) ** 2 < 100:
                        self.processed_mapa[y][x] = 100

            for p in self.inspected_points:
                p = coords_to_point(self.mapa, p)
                print("known", p)
                self.processed_mapa[p[1]][p[0]] = 0

        msg = OccupancyGrid()
        msg.header = self.mapa.header
        msg.header.frame_id = "map"
        msg.info = self.mapa.info

        msg.data = self.processed_mapa.copy().flatten().tolist()
        self.publisher.publish(msg)
        print('published processed map')

    def map_callback(self, data):
        print('got map data')
        self.mapa = deepcopy(data)
        self.update_position()
        self.update_processed_map()

        if self.current_pose != None:
            self.self_point = coords_to_point(self.mapa, np.array(
                [self.current_pose.x, self.current_pose.y]))
            if type(self.point_to_inspect) == np.ndarray:
                point_to_inspect_xy = coords_to_point(
                    self.mapa, self.point_to_inspect)
                if self.processed_mapa[point_to_inspect_xy[1]][point_to_inspect_xy[0]] > -1 or not has_path(self.processed_mapa, point_to_inspect_xy[0], point_to_inspect_xy[1], self.self_point[0], self.self_point[1]):
                    print('Should STOP!!!')
                    self.should_stop = True


def main(args=None):
    global nav

    rclpy.init(args=args)
    mover = MoveForward(1.0)

    try:
        rclpy.spin(mover)
    except BaseException:
        mover.destroy_node()
    print('Move success')
    nav = BasicNavigator()
    susanin = Susanin()

    rclpy.spin(susanin)
    susanin.destroy_node()

    rclpy.shutdown()


if __name__ == '__main__':
    main()