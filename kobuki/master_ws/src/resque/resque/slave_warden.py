import rclpy
from rclpy.node import Node
from websocket import create_connection
from time import sleep
from json import dumps

ws = None

def connect_ws():
    global ws
    try:
        ws = create_connection("ws://localhost:6969/")
    except ConnectionRefusedError:
        sleep(1)
        print('Reconnecting')
        connect_ws()

def send_data(data):
    global ws
    try:
        ws.send(dumps(data))
    except BrokenPipeError:
        ws.close()
        sleep(1)
        connect_ws()       
        send_data(data)

class PosePublisher(Node):

    def __init__(self):
        global ws
        super().__init__('slave_order_publisher')
        
        timer_period = 10  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        ws = None
        try:
            ws = create_connection("ws://192.168.4.5:6969/")
        except:
            pass
        self.i = 0

    def timer_callback(self):
        if (ws == None):
            ws = create_connection("ws://192.168.4.5:6969/")
        else:
            if self.i % 2 == 0:
                data = {"x": 1.0, "y": 0.5, "r": 0.0}
            else:
                data = {"x": -1.0, "y": 0.5, "r": 0.0}
            
            send_data(data)

            self.i += 1


def main(args=None):
    rclpy.init(args=args)

    minimal_publisher = PosePublisher()

    rclpy.spin(minimal_publisher)

    minimal_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()