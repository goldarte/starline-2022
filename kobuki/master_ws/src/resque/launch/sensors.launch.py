import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch_ros.actions import Node
from launch.launch_description_sources import PythonLaunchDescriptionSource

def generate_launch_description():
    # Get the launch directory
    kobuki_dir = get_package_share_directory('kobuki_node')
    kobuki_launch_dir = os.path.join(kobuki_dir, 'launch')

    lidar_dir = get_package_share_directory('rplidar_ros')
    lidar_launch_dir = os.path.join(lidar_dir, 'launch')

    camera_dir = get_package_share_directory('astra_camera')
    camera_launch_dir = os.path.join(camera_dir, 'launch')

    # Specify the actions

    start_kobuki_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(kobuki_launch_dir, 'kobuki.launch.py')
        )
    )

    start_lidar_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(lidar_launch_dir, 'rplidar.launch.py')
        )
    )

    start_camera_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(camera_launch_dir, 'dabai_dw.launch.py')
        )
    )

    base_footprint_tf = Node(
        package    = "tf2_ros",
        executable = "static_transform_publisher",
        arguments  = ["0", "0", "0", "0", "0", "0", "base_link", "base_footprint"],
        output     = "screen"
    )

    lidar_static_tf = Node(
        package    = "tf2_ros",
        executable = "static_transform_publisher",
        arguments  = ["0", "0", "0.44", "3.14159265", "0", "0.0", "base_footprint", "laser"],
        output     = "screen"
    )

    camera_static_tf = Node(
        package    = "tf2_ros",
        executable = "static_transform_publisher",
        arguments  = ["-0.115", "0", "0.3", "0", "0", "0", "base_footprint", "camera_link"],
        output     = "screen"
    )

    # Create the launch description and populate
    ld = LaunchDescription()

    ld.add_action(start_kobuki_cmd)
    ld.add_action(start_lidar_cmd)
    ld.add_action(start_camera_cmd)
    ld.add_action(base_footprint_tf)
    ld.add_action(lidar_static_tf)
    ld.add_action(camera_static_tf)

    return ld
