# Copyright (c) 2018 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""This is all-in-one launch script intended for use by nav2 developers."""

import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess, IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, PythonExpression
from launch_ros.actions import Node


def generate_launch_description():
    # Get the launch directory
    bringup_dir = get_package_share_directory('resque')
    launch_dir = os.path.join(bringup_dir, 'launch')

    # domain_dir = get_package_share_directory('domain_bridge')
    # domain_config_dir = os.path.join(domain_dir, 'config')

    # Create the launch configuration variables
    params_file = LaunchConfiguration('params_file')
    autostart = LaunchConfiguration('autostart')
    start_explore = LaunchConfiguration('explore')

    # Map fully qualified names to relative ones so the node's namespace can be prepended.
    # In case of the transforms (tf), currently, there doesn't seem to be a better alternative
    # https://github.com/ros/geometry2/issues/32
    # https://github.com/ros/robot_state_publisher/pull/30
    # TODO(orduno) Substitute with `PushNodeRemapping`
    #              https://github.com/ros2/launch_ros/issues/56
    remappings = [('/tf', 'tf'),
                  ('/tf_static', 'tf_static')]

    #os.environ['ROS_DOMAIN_ID'] = '6'

    # ros_domain_export_cmd = ExecuteProcess(
    #     cmd=['bash', 'export', 'ROS_DOMAIN_ID=6'],
    #     env={'ROS_DOMAIN_ID': '6'}
    #     )

    declare_params_file_cmd = DeclareLaunchArgument(
        'params_file',
        default_value=os.path.join(bringup_dir, 'params', 'nav2_params.yaml'),
        description='Full path to the ROS2 parameters file to use for all launched nodes')

    declare_autostart_cmd = DeclareLaunchArgument(
        'autostart', default_value='true',
        description='Automatically startup the nav2 stack')

    # declare_rviz_config_file_cmd = DeclareLaunchArgument(
    #     'rviz_config_file',
    #     default_value=os.path.join(
    #         bringup_dir, 'configs', 'nav2.rviz'),
    #     description='Full path to the RVIZ config file to use')

    declare_start_explore_cmd = DeclareLaunchArgument(
        'explore',
        default_value='False',
        description='Start explore_lite algorithm')

    # Specify the actions
    
    # rviz_cmd = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource(
    #         os.path.join(launch_dir, 'rviz_launch.py')),
    #     launch_arguments={'namespace': '',
    #                       'use_namespace': 'False',
    #                       'rviz_config': rviz_config_file}.items())

    sensors_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(launch_dir, 'sensors.launch.py')
        )
    )

    nav2_bringup_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(launch_dir, 'bringup_launch.py')),
        launch_arguments={'namespace': '',
                          'use_namespace': 'false',
                          'use_sim_time': 'false',
                          'slam': 'True',
                          'params_file': params_file,
                          'autostart': autostart}.items())

    explore_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(launch_dir, 'explore.launch.py')),
        condition=IfCondition(start_explore)
        )

    statem_start_cmd = Node(
        package='resque', executable='resc_statem'
    )
    detector = Node(
        package='resque', executable='slave_detector'
    )
    # domain_bridge_cmd = ExecuteProcess(
    #    cmd=['ros2', 'run', 'domain_bridge', 'domain_bridge', os.path.join(domain_dir, 'config', 'config.yaml')])

    # pcl_to_scan_cmd = Node(
    #     package='pointcloud_to_laserscan', executable='pointcloud_to_laserscan_node',
    #     remappings=[('cloud_in', '/camera/points'),
    #                 ('scan', '/scan2')],
    #     parameters=[{
    #         'target_frame': 'base_link',
    #         'transform_tolerance': 0.01,
    #         'min_height': 0.02,
    #         'max_height': 0.2,
    #         'angle_min': -1.5708,  # -M_PI/2
    #         'angle_max': 1.5708,  # M_PI/2
    #         'angle_increment': 0.0087,  # M_PI/360.0
    #         'scan_time': 0.3333,
    #         'range_min': 1.0,
    #         'range_max': 6.0,
    #         'use_inf': True,
    #         'inf_epsilon': 1.0
    #     }],
    #     name='pointcloud_to_laserscan'
    #     )

    # pose_broadcast_node = Node(
    #     package='resque',
    #     executable='slave_warden',
    # )

    # Create the launch description and populate
    ld = LaunchDescription()

    # Declare the launch options
    # ld.add_action(ros_domain_export_cmd)
    ld.add_action(declare_params_file_cmd)
    ld.add_action(declare_autostart_cmd)
    # ld.add_action(declare_rviz_config_file_cmd)
    ld.add_action(declare_start_explore_cmd)

    # ld.add_action(pcl_to_scan_cmd)
    # ld.add_action(sensors_cmd)
    ld.add_action(nav2_bringup_cmd)
    # ld.add_action(rviz_cmd)
    ld.add_action(explore_cmd)
    ld.add_action(detector)
    # ld.add_action(pose_broadcast_node)
    # ld.add_action(domain_bridge_cmd)
    # ld.add_action(statem_start_cmd)

    return ld
