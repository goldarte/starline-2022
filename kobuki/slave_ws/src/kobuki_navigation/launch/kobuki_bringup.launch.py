import os
import yaml

from ament_index_python.packages import get_package_share_directory

import launch.actions
from launch.actions import IncludeLaunchDescription
from launch.actions import DeclareLaunchArgument

from launch import LaunchDescription
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration
from launch.launch_description_sources import PythonLaunchDescriptionSource


def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time', default='false')

    kobuki_navigation_path = get_package_share_directory('kobuki_navigation')

    kobuki_share_dir = get_package_share_directory('kobuki_node')
    kobuki_params_file = os.path.join(
        kobuki_share_dir, 'config', 'kobuki_node_params.yaml')
    with open(kobuki_params_file, 'r') as f:
        kobuki_params = yaml.safe_load(f)['kobuki_ros_node']['ros__parameters']

    kobuki_urdf = os.path.join(kobuki_navigation_path, 'urdf', 'kobuki_carto.urdf')

    return LaunchDescription([
        DeclareLaunchArgument(
            'use_sim_time',
            default_value='false',
            description='Use simulation (Gazebo) clock if true'),

        Node(
            package='kobuki_node',
            executable='kobuki_ros_node',
            output='screen',
            parameters=[kobuki_params]),

        Node(
            name='rplidar_composition',
            package='rplidar_ros',
            executable='rplidar_composition',
            output='screen',
            parameters=[{
                'serial_port': '/dev/rplidar',
                'serial_baudrate': 115200,  # A1 / A2
                # 'serial_baudrate': 256000, # A3
                'frame_id': 'laser',
                'inverted': False,
                'angle_compensate': True,
            }],
        ),

        IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                [kobuki_navigation_path, '/launch/robot.launch.py']),
            launch_arguments={'use_sim_time': use_sim_time}.items(),
        ),
    ])
