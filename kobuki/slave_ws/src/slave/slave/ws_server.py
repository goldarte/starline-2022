import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from geometry_msgs.msg import Twist, PoseStamped
from nav_msgs.msg import Odometry
import numpy as np
import math
import websocket


def on_message(ws, message):
    print('MESSAGE')
    print(message)

class WSServer(Node):
    def __init__(self):
        super().__init__('ws_server')
        websocket.enableTrace(True)
        ws = websocket.WebSocketApp("ws://localhost:6969", # 192.168.4.5
                                on_message = on_message)

        ws.run_forever()

def main(args=None):
    rclpy.init(args=args)
    ws_node = WSServer()

    rclpy.spin(ws_node)

    image_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
