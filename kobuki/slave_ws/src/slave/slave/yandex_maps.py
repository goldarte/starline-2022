import logging
import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from geometry_msgs.msg import Twist, PoseStamped
from nav_msgs.msg import Odometry
import numpy as np
import math
import threading


from time import sleep
from websocket_server import WebsocketServer
from json import loads

def euler_from_quaternion(x, y, z, w):
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)

    return roll_x, pitch_y, yaw_z


def normilize_angle(_a):
    a = _a
    while a > math.pi:
        a -= 2 * math.pi
    while a < -math.pi:
        a += 2 * math.pi
    return a

server = WebsocketServer(host='0.0.0.0', port=6969, loglevel=logging.INFO)

def thread_function():
    global server
    server.run_forever()


class GoalSubscriber(Node):

    def __init__(self):
        global server
        super().__init__('GOAL_subscriber')
        self.publisher = self.create_publisher(Twist, '/commands/velocity', 10)
        # self.publisher = self.create_publisher(Twist, '/cmd_vel', 10)
#        self.subscription = self.create_subscription(
#            PoseStamped,
#            '/goal_pose',
#            self.listener_callback,
#            qos_profile_sensor_data)
        self.busy = False 
        self.points = []
        def on_message(a, b, msg):
            print(msg)
            self.points = loads(msg)
            # self.listener_callback(loads(msg))
        server.set_fn_message_received(on_message)
        self.th = threading.Thread(target=thread_function)
        self.th.start()
        self.odom_subscription = self.create_subscription(
            Odometry,
            '/odom',
            self.odometry_callback,
            qos_profile_sensor_data)

        self.target_pose = None
        self.odo_pose = None

    def odometry_callback(self, data):
        if not self.busy:
            if len(self.points) > 0:
                self.listener_callback(self.points.pop())
                self.busy = True
        self.odo_pose = np.array(
            [data.pose.pose.position.x, data.pose.pose.position.y])
        # print(self.odo_pose)
        orientation = np.array([data.pose.pose.orientation.x, data.pose.pose.orientation.y,
                               data.pose.pose.orientation.z, data.pose.pose.orientation.w])
        self.odo_angle = normilize_angle(
            euler_from_quaternion(*orientation)[2])

        if type(self.target_pose) == np.ndarray:
            movement = Twist()
            if self.dist_to_travel == 0.0 and self.target_angle == 0.0:
                self.target_pose = None
                movement.linear.x = 0.0
                movement.angular.z = 0.0
            else:
                diff = self.target_angle - \
                    normilize_angle(self.odo_angle - self.start_angle)
                diff = normilize_angle(diff)
                if abs(diff) > 0.005 and not self.start_angle_reached:
                    # print(diff, self.odo_angle, self.start_angle)
                    if normilize_angle(self.odo_angle - normilize_angle(self.start_angle + self.target_angle)) > 0:
                        movement.angular.z = -max(min(diff * 2, 0.8), 0.4)
                    else:
                        movement.angular.z = max(min(diff * 2, 0.8), 0.4)

                    movement.linear.x = 0.0
                else:
                    self.start_angle_reached = True
                    dist = self.dist_to_travel - \
                        np.linalg.norm(self.odo_pose-self.start_pose)
                    if dist > 0.01 and not self.dist_reached:
                        # print(dist)
                        movement.linear.x = max(min(dist * 0.3, 1), 0.15)
                        movement.angular.z = 0.0
                    else:
                        self.dist_reached = True
                        diff = self.end_angle - \
                        normilize_angle(self.odo_angle - self.start_angle)
                        diff = normilize_angle(diff)
                        if abs(diff) > 0.005:
                            # print(diff, self.odo_angle, self.start_angle)
                            movement.linear.x = 0.0

                            if normilize_angle(self.odo_angle - normilize_angle(self.start_angle + self.end_angle)) > 0:
                                movement.angular.z = -max(min(diff * 2, 0.8), 0.4)
                            else:
                                movement.angular.z = max(min(diff * 2, 0.8), 0.4)
                        else:
                            self.target_pose = None
                            self.busy = False
                            movement.linear.x = 0.0
                            movement.angular.z = 0.0

            self.publisher.publish(movement)

    def listener_callback(self, data):
        if type(self.odo_pose) != np.ndarray:
            return
        print('Got new goal!')
        self.start_angle_reached = False
        self.dist_reached = False
        self.start_pose = self.odo_pose
        self.start_angle = self.odo_angle
        self.target_pose = np.array(
            [data["y"], data["x"]])
        self.dist_to_travel = np.linalg.norm(self.target_pose)
        self.target_angle = normilize_angle(math.atan2(
            self.target_pose[0], self.target_pose[1]))
        self.end_angle = data["r"]


def main(args=None):
    rclpy.init(args=args)
    image_subscriber = GoalSubscriber()

    rclpy.spin(image_subscriber)

    image_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
