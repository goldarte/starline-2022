#!/bin/bash

# Written by Nikolay Dema <ndema2301@gmail.com>, September 2022
# Дополнено Артуром Голубцовым <goldartt@gmail.com>, Ноябрь 2022

SIM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
REPO_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

xhost +local:docker > /dev/null || true

IMG_NAME="althack/ros2"
IMG_TAG="galactic-gazebo"
GPU_FLAG=""

### Check if NVIDIA GPU flag is needed ----------------------------------- #

if [ -n "$(which nvidia-smi)" ] && [ -n "$(nvidia-smi)" ]; then
    GPU_FLAG=(--gpus all)
    IMG_TAG="galactic-cuda-gazebo-nvidia"
else
    IMG_TAG="galactic-gazebo"
fi

mkdir -p ${SIM_ROOT}/docker/.vscode-server

### DOCKER RUN ----------------------------------------------------------- #

docker run  ${GPU_FLAG[@]} \
            -d -ti --rm \
            -e "DISPLAY" \
            -e "QT_X11_NO_MITSHM=1" \
            -e XAUTHORITY \
            -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
            -v /etc/localtime:/etc/localtime:ro \
            -v ${REPO_ROOT}:/home/robot/starline_2022 \
            -v ${SIM_ROOT}/docker/.vscode-server:/home/robot/.vscode-server \
            --net=host \
            --privileged \
            --name "hsl_2022" ${IMG_NAME}:${IMG_TAG}-hsl2022
