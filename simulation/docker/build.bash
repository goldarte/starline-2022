#!/bin/bash

SIM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

IMG_NAME="althack/ros2"
IMG_TAG="galactic-cuda-gazebo-nvidia"

# Добавляем в образ пользователя с таким же USER_ID как и у хоста,
# чтобы сохранить права доступа к файловой системе.

USER_UID=$(id -u)

### Check for NVIDIA GPU

if [ -n "$(which nvidia-smi)" ] && [ -n "$(nvidia-smi)" ]; then
    IMG_TAG="galactic-cuda-gazebo-nvidia"
else
    IMG_TAG="galactic-gazebo"
fi

set -x

docker build -t ${IMG_NAME}:${IMG_TAG}-hsl2022 -f ${SIM_ROOT}/docker/Dockerfile ${SIM_ROOT} \
    --network=host \
    --build-arg from=${IMG_NAME}:${IMG_TAG} \
    --build-arg USER_UID=${USER_UID}
