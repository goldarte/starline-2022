import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge
from rclpy.qos import qos_profile_sensor_data
from pyzbar import pyzbar
from std_msgs.msg import String

all_messages = set()
current_len = 0
i = 0

class ImageSubscriber(Node):

  def __init__(self):

    super().__init__('image_subscriber')
    self.publisher_ = self.create_publisher(String, '/detected_qrs', 10)
    timer_period = 1  # seconds
    self.timer = self.create_timer(timer_period, self.timer_callback)
    self.subscription = self.create_subscription(
      Image, 
      '/camera/image_raw', 
      self.listener_callback, 
      qos_profile_sensor_data)
    self.subscription # prevent unused variable warning
      
    self.br = CvBridge()
   
  def timer_callback(self):
    msg = String()
    msg.data = str(sorted(list(all_messages)))
    self.publisher_.publish(msg)

  def listener_callback(self, data):

    global i
    i += 1
    if i == 4:
      i = 0
    else:
      return


    #self.get_logger().info('Receiving video frame')
 
    current_frame = self.br.imgmsg_to_cv2(data)
    gray = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)
    #frame = cv2.threshold(frame, 20, 255, cv2.THRESH_BINARY)[1]
    barcodes = pyzbar.decode(gray)
    for barcode in barcodes:
        x, y , w, h = barcode.rect
        barcode_info = barcode.data.decode('utf-8')
        all_messages.add(barcode_info)
        #print('barcode info', barcode_info)
  
def main(args=None):
  
  rclpy.init(args=args) 
  image_subscriber = ImageSubscriber()
  
  rclpy.spin(image_subscriber)
  
  image_subscriber.destroy_node() 
  rclpy.shutdown()
  
if __name__ == '__main__':
  main()
