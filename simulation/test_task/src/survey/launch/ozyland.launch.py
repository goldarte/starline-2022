#!/usr/bin/env python3

# Written by Nikolay Dema <ndema2301@gmail.com>, September 2022

import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription

from launch.actions import ExecuteProcess

from launch_ros.actions import Node


def generate_launch_description():
     pkg_path = get_package_share_directory("survey")

     os.environ["GAZEBO_MODEL_PATH"] = os.path.join(pkg_path, "models")

     world_path = os.path.join(pkg_path, "worlds", "ozyland.world")

     return LaunchDescription([

          ExecuteProcess(output = "screen",
                       cmd    = ["gazebo",
                                 "--verbose",
                                 "-s", "libgazebo_ros_init.so",
                                 world_path]),

          Node(package    = "tf2_ros",
               executable = "static_transform_publisher",
               arguments  = ["0", "0", "0.775", "0", "0", "0", "base_link", "lidar"],
               output     = "screen"),

          Node(package    = "tf2_ros",
               executable = "static_transform_publisher",
               arguments  = ["0.42", "0", "1.75", "-1.570796327", "0.0", "-1.870796327", "base_link", "camera"],
               output     = "screen"),

          Node(package    = "tf2_ros",
               executable = "static_transform_publisher",
               arguments  = ["0.0", "0", "0.01", "0.0", "0.0", "0.0", "base_footprint", "base_link"],
               output     = "screen")

    ])

